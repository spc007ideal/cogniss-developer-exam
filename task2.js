const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")
const getAcronym = async function() {
	try {
		let acronym = randomstring.generate({length: 3, charset: 'alphabetic'}).toUpperCase();
		const response = await got(inputURL+acronym);
		console.log("got data for acronym", acronym);
		console.log("add returned data to definitions array");
		output.definitions.push(JSON.parse(response.body));
		if (output.definitions.length < 10) {
			console.log("calling looping function again");
			await getAcronym();
		}
	} catch (error){
		console.log(error.response.body)
	}
}
console.log("calling looping function");
(async ()=>{
	await getAcronym();
	console.log("saving output file formatted with 2 space indenting");
	jsonfile.writeFile(outputFile, output, {spaces: 2}, function(err) {
		console.log("All done!");
	});
})();