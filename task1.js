const jsonfile = require("jsonfile");
const randomstring = require("randomstring");

const inputFile = "input2.json";
const outputFile = "output2.json";
const suffix = "@gmail.com";

function generateEmail(inputStringArray){
	let email = [];
	inputStringArray.forEach( str => {
		email.push(str.split("").reverse().join("") + randomstring.generate(5) + suffix);
	});
	console.log("Generate Email Done !");
	return email;
}

function writeOutputFile(outputContent){
	jsonfile.writeFile(outputFile, outputContent, {spaces: 2}, function(err) {
		console.log("All done!");
	});
}

jsonfile.readFile(inputFile)
	.then(obj => generateEmail(obj.names))
	.then(array => writeOutputFile({emails: array}))
	.catch(error => console.log(error))